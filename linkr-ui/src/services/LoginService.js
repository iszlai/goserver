import $ from 'jquery'

export default {
    submit(user, password, suc, err) {
        var data = {}
        data.name = user
        data.password = password
        var loc = window.location.protocol + '//' + window.location.hostname + ':8090' + '/login'
        console.log('Calling:' + loc)
        $.ajax({
            type: 'POST',
            url: loc,
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(suc).fail(err)

        // $.post('http://localhost.ms.com:8090/login', JSON.stringify(data), null, 'json').done(suc).fail(err)
    },

    register(user, password, suc, err) {
        var data = {}
        data.name = user
        data.password = password
        var loc = window.location.protocol + '//' + window.location.hostname + ':8090' + '/register'
        console.log('Calling:' + loc)
        $.ajax({
            type: 'POST',
            url: loc,
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(suc).fail(err)
    }
} 
