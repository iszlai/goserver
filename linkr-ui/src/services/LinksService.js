import $ from 'jquery'

export default {
    submit(user, link, suc, err) {
        var encodedLink = window.btoa(link)   
        var base = window.location.protocol + '//' + window.location.hostname + ':8090' + '/links/' + user + '/' + encodedLink
        console.log('Calling:' + base)
        $.ajax({
            type: 'GET',
            url: base
        }).done(suc).fail(err)

        // $.post('http://localhost.ms.com:8090/login', JSON.stringify(data), null, 'json').done(suc).fail(err)
    },

    getLinksForUser(user,suc, err) {
        var base = window.location.protocol + '//' + window.location.hostname + ':8090' + '/user/' + user + '/links'
        console.log('Calling:' + base)
         $.ajax({
            type: 'GET',
            url: base
        }).done(suc).fail(err)
    }

} 
