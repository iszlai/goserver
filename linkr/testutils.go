package main

import (
	"fmt"
	"testing"
)

func assertEqual(t *testing.T, a interface{}, b interface{}) {
	if a == b {
		return
	}

	message := fmt.Sprintf("\n %v != %v", a, b)
	t.Fatal(message)
}

func assertNotEqual(t *testing.T, a interface{}, b interface{}) {
	if a != b {
		return
	}

	message := fmt.Sprintf("\n %v == %v", a, b)
	t.Fatal(message)
}
func assertEqualShould(t *testing.T, a interface{}, b interface{}, text string) {
	if a == b {
		return
	}

	message := fmt.Sprintf("\n %s %v != %v", text, a, b)
	t.Fatal(message)
}

func assertNotEqualShould(t *testing.T, a interface{}, b interface{}, text string) {
	if a != b {
		return
	}
	message := fmt.Sprintf("\n %s %v == %v", text, a, b)
	t.Fatal(message)
}
