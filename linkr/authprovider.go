package main

import (
	"log"

	"github.com/robbert229/jwt"
	"golang.org/x/crypto/bcrypt"
)

type AuthProvider struct {
	Dao  *UserDao `inject:""`
	Algo *jwt.Algorithm
}

func (ap *AuthProvider) createUser(userDTO User) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userDTO.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}

	ap.Dao.insertUser(userDTO.Name, string(hashedPassword))
}

func (ap *AuthProvider) checkCredentials(userDTO User) bool {

	fetchedUser := ap.Dao.findUser(userDTO.Name)
	err := bcrypt.CompareHashAndPassword([]byte(fetchedUser.Password), []byte(userDTO.Password))
	return err == nil // nil means it is a match

}

func (ap *AuthProvider) GetToken(user User) string {

	log.Println("Creating token for user", user.Name)

	claims := jwt.NewClaim()
	claims.Set("Role", "User")
	claims.Set("Username", user.Name)
	token, err := ap.Algo.Encode(claims)
	if err != nil {
		log.Println("Not a valid token")
		return ""
	}
	return token
}

func (ap *AuthProvider) CheckToken(token string) string {

	log.Printf("Checking token %s", token)

	claims, _ := ap.Algo.Decode(token)
	if ap.Algo.Validate(token) == nil {
		user, _ := claims.Get("Username")
		return user.(string)
	}
	return ""
}
