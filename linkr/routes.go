package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"encoding/base64"

	"github.com/gorilla/mux"
)

type RegisterHandler struct {
	Auth *AuthProvider
	ud   *UserDao
	pd   *PagesDao
}

func (l *RegisterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("Register user")
	var user User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		log.Println("got bad request")
		w.WriteHeader(http.StatusBadRequest)
	} else {
		l.Auth.createUser(user)
		w.WriteHeader(http.StatusOK)
	}

}

func (l *RegisterHandler) Login(w http.ResponseWriter, r *http.Request) {
	log.Println("Login user")
	var user User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		log.Println("got bad request")
		w.WriteHeader(http.StatusBadRequest)
	} else {
		isValidUser := l.Auth.checkCredentials(user)
		if isValidUser {
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, l.Auth.GetToken(user))
		} else {
			w.WriteHeader(http.StatusForbidden)
		}

	}

}

func GetAllUsers(response http.ResponseWriter, request *http.Request) {
	log.Println("GetAllUsers")
	users := Users{User{Name: "lehel", Password: "das"}, User{"das", "ldas"}}
	json.NewEncoder(response).Encode(users)
}

func (l *RegisterHandler) GetUser(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	user := vars["userId"]
	found := l.ud.findUser(user)
	log.Printf("getting user [%s]", found)
	json.NewEncoder(response).Encode(found)
}

func (l *RegisterHandler) InsertURL(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	user := vars["userId"]
	url := vars["url"]
	log.Printf("user [%s] inserted [%s]", user, url)
	s, _ := base64.StdEncoding.DecodeString(url)
	l.pd.InsertURL(string(s[:]), user)
}

func (l *RegisterHandler) GetLinks(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	user := vars["userId"]
	log.Printf("user [%s] getting links", user)
	links := l.pd.GetLinksForUser(user)
	json.NewEncoder(response).Encode(links)
}

func (l *RegisterHandler) SearchLinks(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	user := vars["userId"]
	criteria := vars["criteria"]
	log.Printf("user [%s] searching for [%s]", user, criteria)
	links := l.pd.SearchLinks(user, criteria)
	json.NewEncoder(response).Encode(links)
}
