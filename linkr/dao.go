package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/advancedlogic/GoOse"
)

type UserDao struct {
	database *sql.DB
}

func (ud *UserDao) dbconnectionStatus() bool {
	var res int
	err := ud.database.QueryRow("select count(*) from login").Scan(&res)
	if err != nil {
		log.Println(err)
	}
	return -1 < res
}

func (ud *UserDao) checkUser(username string, password string) bool {
	var res string
	err := ud.database.QueryRow("select password from login where username=$1", username).Scan(&res)
	if err != nil {
		log.Println(err)
	}
	return res == password
}

func (ud *UserDao) findUser(username string) User {
	var fetchedUsername, password string
	err := ud.database.QueryRow("select username,password from login where username=$1", username).Scan(&fetchedUsername, &password)
	if err != nil {
		log.Println(err)
	}

	return User{Name: username, Password: password}
}

func (ud *UserDao) insertUser(username string, password string) bool {
	stmt, err := ud.database.Prepare("insert into login (username,password) values ($1,$2)")
	if err != nil {
		log.Println(err)
	}
	_, err2 := stmt.Exec(username, password)
	return err2 == nil
}

func DBInit(host string, port int, dbuser string, password string) (*sql.DB, error) {

	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%d/postgres?sslmode=disable", dbuser, password, host, port)
	log.Println("connection string %s", connectionString)
	db, err := sql.Open("postgres", connectionString)
	return db, err
}

type PagesDao struct {
	database *sql.DB
}

func (pd *PagesDao) GetLinksForUser(username string) []Links {
	var id, url, title string
	rows, err := pd.database.Query("select id,url,title from links where username=$1", username)

	if err != nil {
		log.Println(err)
	}

	var links []Links
	for rows.Next() {
		err := rows.Scan(&id, &url, &title)
		if err != nil {
			log.Println(err)
		}
		links = append(links, Links{ID: id, Title: "true", URL: url})
	}
	return links
}

func (pd *PagesDao) InsertURL(URL string, username string) {
	//var title string
	id := hash(URL)
	g := goose.New()
	article, _ := g.ExtractFromURL(URL)

	pd.database.Exec(`INSERT INTO "pages" ("id", "content") VALUES ($1, $2);`, id, article.CleanedText)
	pd.database.Exec(`INSERT INTO "links" ("id", "url", "title", "username") VALUES ($1, $2, $3, $4);`, id, URL, article.Title, username)
}

func (pd *PagesDao) SearchLinks(username string, criteria string) []Links {
	query := `
select links.id,links.url,links.title
from links 
left join pages on links.id=pages.id
where username=$1
and pages.content @@ to_tsquery($2);
`
	var id, url, title string
	rows, err := pd.database.Query(query, username, criteria)

	if err != nil {
		log.Println(err)
	}

	var links []Links
	for rows.Next() {
		err := rows.Scan(&id, &url, &title)
		if err != nil {
			log.Println(err)
		}
		links = append(links, Links{ID: id, Title: "true", URL: url})
	}
	return links
}
