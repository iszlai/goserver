package main

import (
	"testing"

	"github.com/robbert229/jwt"
)

func TestCreateToken(t *testing.T) {
	algo := jwt.HmacSha256("secret")
	au := AuthProvider{Algo: &algo}
	res := au.GetToken(User{Name: "lehel", Password: "dummy"})
	assertNotEqual(t, res, "")

}

func TestCreateTokenRight(t *testing.T) {
	algo := jwt.HmacSha256("secret")
	au := AuthProvider{Algo: &algo}
	res := au.GetToken(User{Name: "geza", Password: "dummy"})
	uname := au.CheckToken(res)
	assertEqual(t, uname, "geza")
}
