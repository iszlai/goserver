package main

import (
	"log"
	"net/http"

	"database/sql"

	//	"github.com/facebookgo/inject"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/robbert229/jwt"
	"github.com/spf13/viper"
)

func getDBInstance() (*sql.DB, error) {
	host := viper.GetString("development.dbhost")
	port := viper.GetInt("development.dbport")
	dbuser := viper.GetString("development.dbuser")
	dbpassword := viper.GetString("development.dbpassword")
	return DBInit(host, port, dbuser, dbpassword)
}
func main() {
	log.Println("Starting")

	viper.SetConfigName("app")
	viper.AddConfigPath("config")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal("Config file not found...")
	} else {
		devServer := viper.GetString("development.server")
		log.Println(devServer)
	}
	db, err := getDBInstance()
	if err != nil {
		log.Fatal("DB connect error", err)
	}

	log.Println("Initilizing context")
	algo := jwt.HmacSha256(viper.GetString("development.jwtkey"))
	userDao := UserDao{database: db}
	pagesDao := PagesDao{database: db}
	authProvider := AuthProvider{Dao: &userDao, Algo: &algo}
	register := RegisterHandler{&authProvider, &userDao, &pagesDao}
	log.Printf("Checking database connect [%t]", userDao.dbconnectionStatus())
	log.Println("Finished context#")

	//headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	//originsOk := handlers.AllowedOrigins([]string{"*"})
	//methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	router := mux.NewRouter()
	router.HandleFunc("/users", GetAllUsers)
	router.HandleFunc("/user/{userId}", register.GetUser)
	router.HandleFunc("/user/{userId}/links", register.GetLinks)
	router.HandleFunc("/links/search/{userId}/{criteria}", register.SearchLinks)
	router.HandleFunc("/links/{userId}/{url}", register.InsertURL)
	router.HandleFunc("/register", register.ServeHTTP)
	router.HandleFunc("/login", register.Login)
	http.Handle("/", &MyServer{router})
	log.Fatal(http.ListenAndServe(":8090", nil))

}

type MyServer struct {
	r *mux.Router
}

func (s *MyServer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if origin := req.Header.Get("Origin"); origin != "" {
		rw.Header().Set("Access-Control-Allow-Origin", origin)
		rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		rw.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}
	// Stop here if its Preflighted OPTIONS request
	if req.Method == "OPTIONS" {
		return
	}
	// Lets Gorilla work
	s.r.ServeHTTP(rw, req)
}
