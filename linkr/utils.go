package main

import "crypto/sha256"
import "encoding/hex"

func hash(toHash string) string {
	h := sha256.New()
	h.Write([]byte(toHash))
	return hex.EncodeToString(h.Sum(nil))
}
