#!/bin/bash

docker start myPostgres
#docker create -v /var/lib/postgresql/data --name postgres-data busybox
#docker run --name myPostgres -p 5432:5432 -e POSTGRES_PASSWORD=asecurepassword -d --volumes-from postgres-data postgres
# docker run -it --rm --link myPostgres:postgres postgres psql -h postgres -U postgres
#go get ./...

#Deploying changes

#When you make changes to your app code, you’ll need to rebuild your image and recreate your app’s containers. To redeploy a service called web, you would use:
#$ docker-compose build web
#$ docker-compose up --no-deps -d web

# Backup your databases

# docker exec -t your-db-container pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql

# Restore your databases

# cat your_dump.sql | docker exec -i your-db-container psql -U postgres