package main

type User struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

type Users []User

type Links struct {
	ID    string `json:"id"`
	Title string `json:"title"`
	URL   string `json:"url"`
}
